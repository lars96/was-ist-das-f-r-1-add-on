//via http://stackoverflow.com/questions/10730309/find-all-text-nodes-in-html-page#answer-10730777
function textNodesUnder(el) {
  var node;
  var textNodes = [];
  var walk = document.createTreeWalker(el, NodeFilter.SHOW_TEXT, null, false);
  while (node = walk.nextNode()) {
    textNodes.push(node);
  }
  return textNodes;
}

//document.body may be null due to redirects. Google search results for example...
if (document.body != null){
  textNodesUnder(document.body).forEach(function(e) {
    e.textContent = e.textContent.replace(/\be(in|ine|inen|iner|inem|ins)\b/gi, '1');
  });
}
